#ifndef DICE_H
#define DICE_H
#include "Die.h"

class Dice
{
public:
    Dice()
    {
        for (int i = 0; i < 5;i++)
        {
            Die* temp = new Die();
            diceSet[i] = temp;
        }
    }
    int* getNumbers()
    {
        int* temp = new int[5];
        for (int i = 0; i < 5;i++)
        {
            temp[i] = diceSet[i]->getNumber();           
        }
        return temp;
    };
    void rollSet()
    {
        for (int i = 0; i < 5;i++)
        {
            diceSet[i]->roll();
        }
    };
private:
    Die* diceSet[5];
};
#endif // DICE_H