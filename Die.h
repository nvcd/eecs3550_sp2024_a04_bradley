#ifndef DIE_H
#define DIE_H

#include <cstdlib> // For rand()
#include <ctime>   // For time()

class Die {
public:
    Die() {
        
        number = (rand() % 6) + 1; // Properly parentheses for clarity
    }

    int getNumber() const { // Mark as const because it doesn't modify the object
        return number;
    }

    void roll() {
        number = (rand() % 6) + 1;
    }

private:
    int number;
};

#endif // DIE_H