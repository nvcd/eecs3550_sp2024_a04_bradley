# EECS3550_SP2024_A04_Bradley
Set up:
pull file from repositiory and run main.exe in terminal


Running:
The game will roll 5 dice for you. After the roll it will display the numbers
and a list of catagories you can choose to place your score in. Once you pick a
catagory the game rolls again and promts you to pick a catagory again. This repeats
until all catagories are full and game is complete. After your point total is
displayed and the game askes if you want to play again.

Structure:
The game programing is broken into 4 different classes. Game starts the game by calling 
score card, displays total points and asks you to play again. Die contains the code to 
create and roll an individual die. Dice creates and manages 5 dice for rolling during the 
game. Score card contains the main game loop, scores the dice and tracks the points earned 
through the game. Score card also promts you with what section you want to place your current roll.

