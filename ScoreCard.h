//Holds a scorecard for YAHTZEE with many helper functions to make the game run smoothly.
//KEY NOTE!!!
//printPossibleScores() must be ran in your program before you run setScore().
//This is an easy change but it seems more practical to require us to show the user the scores before we take their input

#ifndef SCORECARD_H
#define SCORECARD_H
#include <vector>
#include <utility> // For std::pair
#include <string>
#include <iostream> // For std::std::cout
#include <iomanip>
using namespace std;
class ScoreCard
{
public:
    ScoreCard() : numOfKinds(7, 0), scored(16, false){}

    bool setScore(int category) // Returns true if the score was set
    {                                      // Must call printPossibleScores before this function
        // std::cout << "set Score called with category: " << category << std::endl;
        // return false;
        int bonusTotal = 0;    // Holds running total so see if bonus has been reached
        if (printScoreSetFlag) // check that printPossibleScores has been called
        {
            if (category < 15 && category > 0 && !scored[category]) // If the picked category hasn't been scored yet and a valid number
            {
                scores[category].second = printScores[category].second; //Take the calculated score and place it into that category
                scored[category] = true;    //This category has now been scored
                for (int i = 1; i < 15; i++)  //Loop through printScores  
                    printScores[i].second = 0; // reset printscores
                printScoreSetFlag = false;

                int i = 1; //Loop through the first 6 scores and get their total
                for (i = 1; i < 7; i++)
                {
                    if (!(scored[i])) //If one of them hasn't been scored yet break the loop
                        break;
                    bonusTotal += scores[i].second; //Get the total
                }
                if (i == 7) //If all numbers scored 
                {
                    scored[14] = true; //Set the sum score to true 
                    scores[14].second = bonusTotal; //Set sum
                    if (bonusTotal >= 63) //If sum is >= 63 get bonus of 35
                    {
                        scores[15].second = 35;
                    }
                    scored[15] = true; //Bonus has be scored
                }
                return true;// return true score was set
            }
        }
        return false;
    }

    bool scoreCardFull() //Checks if the score card is full/game done
    {
        for (int i = 1; i < 15; i++)
        {
            if (!scored[i])
                return false;
        }
        return true;
    }

    int getCurrentScore() //Get the Players current score
    {
        int total = 0;  //Holds our total
        for (int i = 1; i < 16; i++)
        {
            if (i == 14) continue; //Skip sum duplicate score of the first 6
            total += scores[i].second; //Add each score
        }
        return total;
    }

    void printScoreCard() //Print the scorecard
    {   
        std::cout << "Current Score" << endl;
        for (int i = 1; i < 16; i++)
        {
            std::cout << std::setw(18) << scores[i].first  << " | " << scores[i].second << std::endl;
        }
    }
    void printPossibleScores(int *dice) //Prints the available scores for a player
    { // Takes in a pointer assuming it's to an array of 5 values 1-6
        for (int i = 1; i < 14; i++)
        {
            if(i < 7) numOfKinds[i] = 0;
            printScores[i].second = 0; // reset printscores
        }
        bool fullHouseTwoFlag, threeOfAKindFlag = false; // flags for want is possible
        int straightRoll = 0;                     // Hold how many sequential numbers are in a row
        //Set print scores to zero
        for (int i = 0; i < 5; i++) //Loop through the 5 dice
        {
            numOfKinds[dice[i]]++;  //Add 1 to the number of time this dice was rolled
            if (!scored[12])        // if we haven't used chance add up it's total
                printScores[12].second += dice[i]; 
        }
        for (int i = 1; i < 7; i++) //Loop through the first 6 scores
        {
            if (!scored[i] && numOfKinds[i] > 0) //If not scored
            {
                printScores[i].second = i * numOfKinds[i]; //Grab this dice total and multiple it by it's value(i+1)
            }
            //If there's 2 of a kind set the flag
            if (numOfKinds[i] == 2 && !(scored[9])) 
                fullHouseTwoFlag = true;
            //If there's three of a kind set the flag and calculate the score of it
            if (numOfKinds[i] >= 3 && !(scored[7])) 
            {
                threeOfAKindFlag = true;
                printScores[7].second = i * 3;
            }
            //If there's four of a kind calculate the score of it
            if (numOfKinds[i] >= 4 && !(scored[8]))
            {
                printScores[8].second = i * 4;
            }
            //If there's five of a kind show YAHTZEE score
            if (numOfKinds[i] == 5 && !(scored[13]))
            {
                printScores[13].second = 50;
            }
            //If a nice value have been rolled increment straight counter
            if (numOfKinds[i] > 0)
                straightRoll++;
            //Else reset the straight count
            else if(straightRoll < 4)
                straightRoll = 0;
        }
        //4 straigh is a small straight
        if (straightRoll >= 4 && !(scored[10]))
        {
            printScores[10].second = 30 ;
        }
        //5 straigh is a large straight
        if (straightRoll == 5 && !(scored[11]))
        {
            printScores[11].second = 40;
        }
        //If we had 2 and 3 of a kind we have a full house
        if (fullHouseTwoFlag && threeOfAKindFlag)
        {
            printScores[9].second = 25;
        }
        //Print the layout guide
        std::cout << "     <Category>(n) <Score>" << std::endl;
        //Loop through the possible scores
        for (int i = 1; i < 14; i++)
        {   
            if(!(scored[i])) //If not scored already print it and it's potential value
            std::cout << " | " << setw(18) << printScores[i].first << " " << setw(3) << printScores[i].second << " | " << std::endl;
        }
        std::cout << std::endl;
        printScoreSetFlag = true; //Set printScoreSetFlag to true we calculated the scores
    }

private:
    bool printScoreSetFlag = false;
    std::vector<int> numOfKinds;                // Moved to class level and initialized in constructor
    std::vector<pair<string, int>> printScores = 
        {{"N/A", -1}, {"Ones(1)", 0}, {"Twos(2)", 0}, {"Three(3)", 0}, {"Fours(4)", 0}, {"Fives(5)", 0}, {"Sixes(6)", 0}, {"Three of a Kind(7)", 0},
        {"Four of a Kind(8)", 0}, {"Full House(9)", 0}, {"Small Straight(10)", 0}, {"Large Straight(11)", 0}, {"Chance(12)", 0}, {"YAHTZEE(13)", 0}}; // Moved to class level
    std::vector<bool> scored;              // Flags for if it has been scored
    std::vector<std::pair<std::string, int>> scores =
        {{"N/A", -1}, {"Ones", 0}, {"Twos", 0}, {"Three", 0}, {"Fours", 0}, {"Fives", 0}, {"Sixes", 0}, {"Three of a Kind", 0}, 
        {"Four of a Kind", 0}, {"Full House", 0}, {"Small Straight", 0}, {"Large Straight", 0}, 
        {"Chance", 0}, {"YAHTZEE", 0}, {"Sum", 0}, {"Bonus", 0}}; //
};
#endif